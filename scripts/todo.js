
function init(){

$('li').on('click', function(){
    $(this).toggleClass('done');
  });

  $('.delete').on('click', function(){
      $(this).parent().fadeOut(1000, function(){$(this).parent().remmove()});
  });

  $('.today').on('click', '.toLater', function(){
    $(this).removeClass('toLater').addClass('toToday').html('Move to Today');
    $(this).parent().detach().appendTo('.later-list').toggleClass('done');
   });

  $('.later').on('click', '.toToday' , function(){
    $(this).addClass('toLater').removeClass('toToday').html('Move to Later');
    $(this).parent().detach().appendTo('.today-list').toggleClass('done');
  });

  $('.today').on('click', '.add .add-item', function(){
    $(this).parent().prev().children().last().clone(true).appendTo('.today-list');
    $('.today-list').children().last().children().first().html($(this).siblings('input').val());
    $(this).siblings('input').val('');
  });

  $('.later').on('click', '.add .add-item', function(){
    $(this).parent().prev().children().last().clone(true).appendTo('.later-list');
    $('.later-list').children().last().children().first().html($(this).siblings('input').val());
    $(this).siblings('input').val('');
  });
};