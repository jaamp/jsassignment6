// > Create a Car constructor function, with the following
// properties and methods (determine if it goes on an
// instance vs the prototype):
// – currentSpeed: initially set to zero
// – model: set by constructor function (i.e. "Taurus")
const Car = function(model) {
    this.model = model;
    //this.currentSpeed = speed;
}

//protos
// – accelerate: function, increases current speed by 1
Car.prototype.Accelerate = function() {
    this.currentSpeed++;
};
// – brake: function, decreases current speed by 1
Car.prototype.Brake = function() {
    this.currentSpeed--;
};
// – toString: function, returns a string representation of the object
Car.prototype.toString = function() {
   return "My " + this.model + " is going " + this.currentSpeed +" mph.";
}
Car.prototype.currentSpeed = 0;

const ford = new Car("F150");

ford.Accelerate();
ford.Accelerate();
ford.Accelerate();
ford.Accelerate();
ford.Accelerate();
ford.Accelerate();
console.log(ford.currentSpeed);

ford.Brake();
console.log(ford.currentSpeed);

console.log(ford.toString());


